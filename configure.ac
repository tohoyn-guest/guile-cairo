define(GUILE_CAIRO_CONFIGURE_COPYRIGHT,[[

Copyright (C) 2007, 2011-2012, 2014, 2017, 2020 Andy Wingo <wingo@pobox.com>
Copyright (C) 2011 Patrick Bernaud <patrickb@chez.com>
Copyright (C) 2018 Daniel Llorens <daniel.llorens@bluewin.ch>
Copyright (C) 2018 David Pirotte <david@altosw.be>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program.  If not, see
<http://www.gnu.org/licenses/>.

]])

AC_PREREQ(2.52)

AC_INIT([guile-cairo],
        m4_esyscmd([build-aux/git-version-gen --match v1.11.2\* .tarball-version]),
        [guile-user@gnu.org])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([m4])
AC_COPYRIGHT(GUILE_CAIRO_CONFIGURE_COPYRIGHT)

AM_CONFIG_HEADER(config.h)

AM_INIT_AUTOMAKE([1.14 gnu -Wno-portability -Wno-syntax \
  color-tests serial-tests foreign])
AM_MAINTAINER_MODE([enable])
AM_SILENT_RULES([yes])

AC_SUBST(VERSION, 1.12.0)

AC_ISC_POSIX
AC_PROG_CC
AC_STDC_HEADERS
LT_INIT([disable-static])


if test "x$prefix" = xNONE; then
   AC_MSG_ERROR([No explicit prefix given.

Guile-Cairo requires you to explicitly enter in a prefix when
configuring. This is because the default prefix, /usr/local, is not in
the default guile load path. What you probably want to do is to run

  $0 --prefix=/usr

so that guile can find the guile-cairo modules when they are installed.

If you choose to install to a prefix that is not in guile's load path,
you will need to modify GUILE_LOAD_PATH and LD_LIBRARY_PATH every time
you run guile. (Which is a pain.)])
fi

WARN_CFLAGS=-Wall
AC_ARG_ENABLE([Werror], AC_HELP_STRING([--disable-Werror],[Don't stop the build on errors]),
        [], WARN_CFLAGS="-Wall -Werror")
AC_SUBST(WARN_CFLAGS)

# Optimize the DSO symbol hash table -- see ulrich drepper's paper,
# "how to write shared libraries"
GNULD_LDFLAGS=-Wl,-O1
AC_SUBST(GNULD_LDFLAGS)
AM_LDFLAGS='$(GNULD_LDFLAGS)'
AC_SUBST(AM_LDFLAGS)

#
# Check for Guile
#
GUILE_PKG
GUILE_PROGS
GUILE_FLAGS
AC_SUBST(GUILE_EFFECTIVE_VERSION)

if test "$cross_compiling" != no; then
   GUILE_TARGET="--target=$host_alias"
   AC_SUBST([GUILE_TARGET])
fi

PKG_CHECK_MODULES(CAIRO, cairo >= 1.10.0)
AC_SUBST(CAIRO_LIBS)
AC_SUBST(CAIRO_CFLAGS)

AC_ARG_VAR(CAIRO_XML_DIR, [path to Cairo *.xml documentation])

AC_CONFIG_FILES([env], [chmod +x env])
AC_CONFIG_FILES(
Makefile
guile-cairo.pc
guile-cairo/Makefile
cairo/Makefile
examples/Makefile
tests/Makefile
tests/unit-tests/Makefile
doc/Makefile
)
AC_OUTPUT
